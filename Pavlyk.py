try:
    # a) Введення рядків користувачем та запис у файли TF18_1 і TF18_2
    with open('TF18_1.txt', 'w') as file1, open('TF18_2.txt', 'w') as file2:
        user_input1 = input("Введіть рядок для TF18_1.txt: ")
        user_input2 = input("Введіть рядок для TF18_2.txt: ")

        file1.write(user_input1)
        file2.write(user_input2)

    # б) Переписування вмісту файлів TF18_1 і TF18_2 один у одного з використанням допоміжного файлу TF18_3
    with open('TF18_1.txt', 'r') as file1, open('TF18_2.txt', 'r') as file2, open('TF18_3.txt', 'w') as file3:
        content1 = file1.read()
        content2 = file2.read()

        # Розділення символьних рядків на рядки по 20 символів і запис у допоміжний файл
        for i in range(0, max(len(content1), len(content2)), 20):
            file3.write(content1[i:i+20] + '\n')
            file3.write(content2[i:i+20] + '\n')

    # в) Читання вмісту файлів TF18_1, TF18_2 і друк на екран
    with open('TF18_1.txt', 'r') as file1, open('TF18_2.txt', 'r') as file2, open('TF18_3.txt', 'r') as file3:
        print("TF18_1.txt:")
        print(file1.read())

        print("\nTF18_2.txt:")
        print(file2.read())

        print("\nTF18_3.txt:")
        print(file3.read())

except IOError as e:
    print(f"Помилка відкриття/запису файлу: {e}")
except Exception as e:
    print(f"Сталася невідома помилка: {e}")
